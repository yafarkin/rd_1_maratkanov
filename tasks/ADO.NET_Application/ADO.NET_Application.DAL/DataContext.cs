﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Web;


namespace ADO.NET_Application.DAL
{


    public class DataContext
    {
        public List<Record> data { get; set; }
        public List<string> countryList { get; set; }//Список стран для подгрузки в <select>
        public List<string> cityList { get; set; }//Список городов для подгрузки в <select>

        public string SelectedCountry { get; set; }
        public string SelectedCity { get; set; }

        public DataContext()
        {

        }

        public DataContext(string country)
        {
            SelectedCountry = country;
        }

        public void getConnect()
        {
            // Создание открытого подключения
            using (SqlConnection cn = new SqlConnection())
            {
                cn.ConnectionString = @"Data Source=DESKTOP-AQREFK2\SQLEXPRESS;Initial Catalog=Northwind;" +
                    "Integrated Security=SSPI;Pooling=False";
                
                cn.Open();

                // Работа с базой данных
                data = getData(cn);

                cn.Close();
            }
        }

        public List<Record> getData(SqlConnection cn)
        {
            // Создание объекта команды с помощью конструктора

            string getCountrySQL = "Select Distinct Country From Employees";
            string getCitySQL = "Select Distinct City From Employees";

            SqlCommand myCommand = new SqlCommand("GetEmployeeList", cn);
            SqlCommand countryCommand = new SqlCommand(getCountrySQL, cn);
            SqlCommand cityCommand = new SqlCommand(getCitySQL, cn);


            //if(SelectedCountry==null)
            //{
            //    SelectedCountry = DBNull.Value;
                
            //}

            //if (SelectedCity == null)
            //{
            //    SelectedCity = DBNull.Value.ToString();
            //}

            myCommand.CommandType = CommandType.StoredProcedure;
            if (SelectedCountry == null)
                myCommand.Parameters.Add("@country", SqlDbType.NVarChar).Value = DBNull.Value;
            else
                myCommand.Parameters.Add("@country", SqlDbType.NVarChar).Value = SelectedCountry;


            if (SelectedCity == null)
                myCommand.Parameters.Add("@city", SqlDbType.NVarChar).Value = DBNull.Value;
            else
                myCommand.Parameters.Add("@city", SqlDbType.NVarChar).Value = SelectedCity;

            SqlDataReader dr = myCommand.ExecuteReader();

            List<Record> result = new List<Record>();
            while (dr.Read())
                result.Add(new Record(dr[0].ToString(), dr[1].ToString(), dr[2].ToString() , dr[3].ToString() , dr[4].ToString()));
            dr.Close();

            SqlDataReader drCountry = countryCommand.ExecuteReader();

            countryList = new List<string>();
            while (drCountry.Read())
                countryList.Add(drCountry[0].ToString());
            drCountry.Close();


            SqlDataReader drCity = cityCommand.ExecuteReader();
            cityList = new List<string>();
            while (drCity.Read())
                cityList.Add(drCity[0].ToString());
            drCity.Close();


            return result;
        }

    }
}
