﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADO.NET_Application.DAL
{
    /// <summary>
    /// Класс представляет собой одну запись из набора, который возвращает хранимая процедура
    /// </summary>
    public class Record
    {
        public string EmpName { get; set; }
        public string EmpCountry { get; set; }
        public string EmpCity { get; set; }

        public string OrderName { get; set; }
        public string OrderNumber { get; set; }

        public string SelectedCountry { get; set; }
        public string SelectedCity { get; set; }

        public Record(string empName, string empCountry, string empCity ,string ordName, string ordNumber)
        {
            EmpName = empName;
            EmpCountry = empCountry;
            EmpCity = empCity;

            OrderName = ordName;
            OrderNumber = ordNumber;
        }

        
    }
}
