--�� ��������� ���������� ���������� �������, � ������� ������� ����������� ������ �����������(Employees) � ����������� �� ��� �������(Orders).

CREATE PROCEDURE GetEmployeeList (@country nvarchar(50), @city nvarchar(50))
AS

--���� ��� ��������� ������������
IF @country IS NOT NULL AND @city IS NOT NULL
begin
	--��� ���� ������� �� ������, ������� �� ��������� � ��������� ������
	Select FirstName as '���', Country as '������ ����������', City as '����� ����������' ,ShipName as '�������� ������', OrderID as '����� ������'
	From Employees Join Orders ON Employees.EmployeeID=Orders.EmployeeID
	Where Country = @country AND @city IN (Select City From Employees Where Country = @country)
end
ELSE
	--���� ��� ��������� �����������
	IF @country IS NULL AND @city IS NULL
	begin
		Select FirstName as '���', Country as '������ ����������', City as '����� ����������' ,ShipName as '�������� ������', OrderID as '����� ������'
		From Employees Join Orders ON Employees.EmployeeID=Orders.EmployeeID
	end
	ELSE
		--����� �� ���� �� ���������� ����, � ������ NULL
		IF @country IS NULL
		begin
			Select FirstName as '���', Country as '������ ����������', City as '����� ����������' ,ShipName as '�������� ������', OrderID as '����� ������'
			From Employees Join Orders ON Employees.EmployeeID=Orders.EmployeeID
			Where City = @city
		end
		ELSE
		begin
			Select FirstName as '���', Country as '������ ����������', City as '����� ����������' ,ShipName as '�������� ������', OrderID as '����� ������'
			From Employees Join Orders ON Employees.EmployeeID=Orders.EmployeeID
			Where Country = @country
		end