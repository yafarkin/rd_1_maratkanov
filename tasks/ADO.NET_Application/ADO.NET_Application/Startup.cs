﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ADO.NET_Application.Startup))]
namespace ADO.NET_Application
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
