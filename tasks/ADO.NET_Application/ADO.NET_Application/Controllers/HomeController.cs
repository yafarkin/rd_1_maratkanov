﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ADO.NET_Application.DAL;


namespace ADO.NET_Application.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(string country, string city)
        {

            DataContext model = new DataContext();

            model.SelectedCountry = country;
            model.SelectedCity = city;

            model.getConnect();


            

            List<SelectListItem> countries = new List<SelectListItem>();
            countries.Add(new SelectListItem() { Text = "", Value = "", Selected = true });
           
            foreach (var element in model.countryList)
            
                countries.Add(new SelectListItem() { Text = element.ToString(), Value = element.ToString() });
              
            List<SelectListItem> cities = new List<SelectListItem>();
            cities.Add(new SelectListItem() { Text = "", Value = "", Selected = true });

            
            foreach (var element in model.cityList)
            
                cities.Add(new SelectListItem() { Text = element.ToString(), Value = element.ToString() });
            

            //if (value != null)
            //{
            //    countries.Where(k => k.Value == country.ToString()).First().Selected = true;

            //}

            //if (city != null)
            //{
            //    cities.Where(j => j.Value == city.ToString()).First().Selected = true;
            //}


            ViewBag.MyCountries = countries;
            ViewBag.MyCities = cities;
            return View(model);
            
        }

        /// <summary>
        /// POST-версия метода, возвращающая представление с выбранной страной.
        /// </summary>
        //[HttpPost]
        //public ActionResult Index(string country)
        //{

        //    ViewData["country"] = country;

        //    return View(country);
        //}


        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}