USE [Northwind]
GO

DECLARE	@return_value int

EXEC	@return_value = [dbo].[GetEmployeeList]
		@country = 'UK',
		@city = 'London'

SELECT	'Return Value' = @return_value

GO
