﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_6
{
    class Program
    {
        static void Main(string[] args)
        {
            Hidden h = new Hidden();
            Console.WriteLine(h.first);
            Console.WriteLine(h.second);

            h.Third = "Я private поле third класса Hidden";
            h.Fourth= "Я private поле fourth класса Hidden";

            Console.WriteLine(h.Third);
            Console.WriteLine(h.Fourth);
            Console.ReadLine();

            MyClass my = new MyClass();

            Console.WriteLine(my.Third);
            Console.WriteLine(h.City);
            Console.WriteLine(my.City);
            Console.WriteLine(my.ParentCity);
            Console.WriteLine("\n\nРабота со статическим классом================\n\n");

            Console.WriteLine(MyStaticClass.Name);
            my.ChangeStaticName("Измененное поле name статического класса (меняем классом MyClass)");
            Console.WriteLine(MyStaticClass.Name);
            MyStaticClass.Name = "Опять статика";
            Console.WriteLine(MyStaticClass.Name);

            Console.WriteLine("\n\nИспользование методов расширения---------------------");

            Console.WriteLine(StringExtension.GetWordCount("один два три"));


            Console.ReadLine();


            
        }
    }
}
