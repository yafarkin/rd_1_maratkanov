﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_6
{
    static class StringExtension
    {
        /// <summary>
        /// GetWordCount - подсчитывает количество слов в строке. Слова отделяются друг
        /// от друга с помощью пробела, запятой, или точки с запятой
        /// </summary>
        /// <param name="input_str">
        /// Входная строка, в которой необходимо подсчитать 
        /// количество слов
        /// </param>
        /// <returns></returns>
        public static int GetWordCount(this string input_str)
        {
            string text = input_str.Trim(new char[] {',','.',';','"' });//Убираем лишние знаки
            string[] textArray = text.Split(new char[] { ' ' });
            return textArray.Length;
            
        }
    }
}
