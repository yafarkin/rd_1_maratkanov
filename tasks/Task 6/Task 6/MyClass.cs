﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_6
{
    class MyClass:Hidden
    {
        string third="Я - поле third класса MyClass";
        string fourth = "Я - поле third класса MyClass";
        string city="город Тольятти класса MyClass";

        public string Third
        {
            get
            {
                return third;
            }
            set
            {
                third = value;
            }
        }

        public string Fourth
        {
            get
            {
                return fourth;
            }
            set
            {
                fourth = value;
            }
        }

        public string City
        {
            get
            {
                return city;
            }
            set
            {
                city = value;
            }
        }

        public string ParentCity
        {
            get
            {
                return base.City;
            }
            set
            {
                base.City = value;
            }
        }

        public void ChangeStaticCity(string new_city)
        {
            MyStaticClass.City = new_city;
        }

        public string GetStaticCity(string static_city)
        {
            return MyStaticClass.City;
        }

        public void ChangeStaticName(string new_name)
        {
            MyStaticClass.Name = new_name;
        }

        public string GetStaticName(string static_name)
        {
            return MyStaticClass.Name;
        }

        
    }
}
