﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_6
{
    class Hidden
    {
        public string first="Я поле first класса Hidden";
        public string second="Я поле second класса Hidden";
        string third;
        string fourth;
        string city="Город по-умолчанию класса Hidden";

        public string Third
        {
            get
            {
                return third;
            }
            set
            {
                third = value;
            }
        }

        public string Fourth
        {
            get
            {
                return fourth;
            }
            set
            {
                fourth = value;
            }
        }

        public string City
        {
            get
            {
                return city;
            }
            set
            {
                city = value;
            }
        }
    }
}
