﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_6
{
    class MyStaticClass
    {
        static string name= "Имя по-умолчанию класса MyStaticClass";
        static string city= "Город по-умолчанию класса MyStaticClass";

        public static string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public static string City
        {
            get
            {
                return city;
            }
            set
            {
                city = value;
            }
        }

        
    }
}
