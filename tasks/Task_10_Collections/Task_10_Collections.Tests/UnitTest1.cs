﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Task_10_Collections.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Contains_6_In_2_6_4_true_returned()
        {
            //arrange
            int item = 6;
            

            bool expected = true;

            //act
            MyCollection<int> col = new MyCollection<int>();
            col.Add(2);
            col.Add(6);
            col.Add(4);

            bool actual = col.Contains(item);

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Copy_Col_2_4_to_Ar_0_0_returned_Ar_2_4()
        {
            //arrange
            int[] ar = new int[2];
            int[] expected = new int[2] { 2, 4};


            //act
            MyCollection<int> col = new MyCollection<int>();
            col.Add(2);
            col.Add(4);
            col.CopyTo(ar, 0);



            //Asserted
            CollectionAssert.AreEqual(expected, ar);
            //Assert.AreEqual(expected, ar);
        }

        [TestMethod]
        public void Remove_9_From_Col_2_8_3_9_3_returned_2_8_3_3()
        {
            //arrange
            MyCollection<int> expected = new MyCollection<int> { 2, 8, 3, 3 };

            //act
            MyCollection<int> actual = new MyCollection<int> { 2, 8, 3, 9, 3 };
            actual.Remove(9);//Удаляем 9

            //Asserted
            CollectionAssert.AreEqual(expected.Arr, actual.Arr);


        }


        [TestMethod]
        public void Clear_Col_123_returned_000()
        {
            //arrange
            MyCollection<int> expected = new MyCollection<int> { 3, 4, 5 };
            expected.Clear();

            //act

            MyCollection<int> actual = new MyCollection<int> { 1, 2, 3 };
            actual.Clear();

            //Asserted
            //Assert.AreEqual(expected, actual);
            CollectionAssert.AreEqual(expected.Arr, actual.Arr);
        }

        [TestMethod]
        public void Add_1_3_returned_1_3_collection()
        {
            //arrange
            MyCollection<int> expected = new MyCollection<int>() { 1, 3 };


            //act
            MyCollection<int> actual = new MyCollection<int>();
            actual.Add(1);
            actual.Add(3);

            //Asserted
            CollectionAssert.AreEqual(expected.Arr, actual.Arr);
        }
    }
}
