﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_10_Collections
{
    public class MyCollectionEventArgs
    {
        public enum EventType
        {
            Added,
            Deleted,
            Updated,
            Get
        };

        public readonly EventType eventType;//тип события

        public MyCollectionEventArgs(EventType eventType)
        {
            this.eventType = eventType;
        }
    }
}
