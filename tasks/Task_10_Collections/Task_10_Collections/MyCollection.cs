﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_10_Collections
{
    public class MyCollection<T> : IEnumerable<T>, ICollection<T>
    {
        T[] arr = new T[0];//Наш внутренний массив
        int elementsInArr = 0;
        public event EventHandler<MyCollectionEventArgs> CollectionChanged;//Событие изменения коллекции

        public int Count { get { return elementsInArr; } }
        public T[] Arr { get { return arr; } }
        public bool IsReadOnly { get { return false; } }

        
        //Индексатор
        public T this[int idx]
        {
            get
            {
                if (elementsInArr <= idx)
                    throw new IndexOutOfRangeException();
                CollectionChanged?.Invoke(this,
                    new MyCollectionEventArgs(MyCollectionEventArgs.EventType.Get));
                return arr[idx];
            }
            set
            {
                if (elementsInArr <= idx)
                    throw new IndexOutOfRangeException();
                CollectionChanged?.Invoke(this,
                    new MyCollectionEventArgs(MyCollectionEventArgs.EventType.Updated));
                arr[idx] = value;
            }
        }
        



        //Реализация IEnumerable<int>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }


        public IEnumerator<T> GetEnumerator()
        {
            for (int i = 0; i < elementsInArr; ++i)
                yield return arr[i];
        }

        /// <summary>
        /// Метод добавдяет элемент item в конец коллекции
        /// </summary>
        /// <param name="item">Добавляемый в коллекцию элемент</param>
        public void Add(T item)
        {
            /*
            if (arr.Length == elementsInArr)
            {
                T[] tmpArr = new T[arr.Length + arr.Length / 2];
                arr.CopyTo(tmpArr, 0);
                arr = tmpArr;
            }
            arr[elementsInArr++] = item;
            CollectionChanged?.Invoke(this,
                new MyCollectionEventArgs(MyCollectionEventArgs.EventType.Added));
            */


            if (arr.Length == elementsInArr)
            {
                T[] newArray = new T[arr.Length + 1];//Создание нового массива (на 1 больше старого)
                arr.CopyTo(newArray, 0);//Копирование старого массива в новый
                //newArray[newArray.Length - 1] = item;//Помещение нового значения в конец массива
                arr = newArray;//Замена старого масива на новый
            }
            arr[elementsInArr++] = item;
            CollectionChanged?.Invoke(this,
                new MyCollectionEventArgs(MyCollectionEventArgs.EventType.Added));
        }

        /*
        public void Add(T item)
        {
            if (arr.Length == elementsInArr)
            {
                T[] tmpArr = new T[arr.Length + arr.Length / 2];
                arr.CopyTo(tmpArr, 0);
                arr = tmpArr;
            }
            arr[elementsInArr++] = item;
            CollectionChanged?.Invoke(this,
                new MyCollectionEventArgs(MyCollectionEventArgs.EventType.Added));
        }
        */

        /// <summary>
        /// Метод очистки коллекции от элементов
        /// </summary>
        public void Clear()
        {
            elementsInArr = 0;
            arr = new T[0];
            //Array.Clear(arr, 0, arr.Length);

            CollectionChanged?.Invoke(this,
                new MyCollectionEventArgs(MyCollectionEventArgs.EventType.Deleted));
        }

        /// <summary>
        /// Метод проверяет, содержится ли элемент item в коллекции
        /// </summary>
        /// <param name="item">Элемент, который необходимо найти</param>
        /// <returns></returns>
        public bool Contains(T item)
        {
            for (int i = 0; i < elementsInArr; ++i)
                if (arr[i].Equals(item))
                    return true;
            return false;
        }


        /// <summary>
        /// Метод копирует содержимое коллекции в массив array, переданный в качетве аргумента
        /// </summary>
        /// <param name="array">Массив, в который будут скопированы элементы коллекции</param>
        /// <param name="arrayIndex">Индекс, с котого в массиве будут располагаться элементы коллекции</param>
        public void CopyTo(T[] array, int arrayIndex)
        {
            for (int i = arrayIndex; i < elementsInArr; ++i)
                array[i] = arr[i];
        }


        /// <summary>
        /// Метод удаляет элемент elem из коллекции
        /// </summary>
        /// <param name="elem">Удаляемый элемент</param>
        /// <returns>true - в случае успешного удаления, и false - в случае неуспешного</returns>
        public bool Remove(T elem)
        {
            if (arr.Contains(elem))
            {
                //3 6 7 8 0 4
                //Надо 8 удалить например
                //Новый массив: 3 6 7 0 4
                T[] buf = arr;
                T[] newArray = new T[arr.Length - 1];//Создание нового массива (на 1 меньше старого)

                int i = 0;
                int j = 0;

                while (i < arr.Length && j < newArray.Length)
                {
                    //if (elem != arr[i])
                    if (!elem.Equals(arr[i]))
                    {
                        newArray[j] = arr[i];
                        j++;
                    }

                    i++;

                }
                arr = newArray;
            }

            return false;
        }


        /*
        public bool Remove(T item)
        {
            int idx;
            for (idx = 0; idx < elementsInArr; ++idx)
                if (arr[idx].Equals(item))
                    break;
            if (idx == elementsInArr)
                return false;

            for (int i = idx + 1; i < elementsInArr; ++i)
                arr[i - 1] = arr[i];
            --elementsInArr;
            CollectionChanged?.Invoke(this,
                new MyCollectionEventArgs(MyCollectionEventArgs.EventType.Deleted));
            return true;
        }*/

        
        /// <summary>
        /// Метод распечатки коллекции на экран
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        static void printEvent(object sender, MyCollectionEventArgs e)
        {
            Console.Write("===Event: ");
            switch (e.eventType)
            {
                case MyCollectionEventArgs.EventType.Added:
                    Console.Write("New element added");
                    break;

                case MyCollectionEventArgs.EventType.Deleted:
                    Console.Write("Element(s) deleted");
                    break;

                case MyCollectionEventArgs.EventType.Get:
                    Console.Write("Got element");
                    break;

                case MyCollectionEventArgs.EventType.Updated:
                    Console.Write("Collection updated");
                    break;
            }
            Console.WriteLine("===");

        }
    }
}
