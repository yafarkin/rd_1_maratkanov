﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_10_Collections
{
    class Program
    {
        static void Main(string[] args)
        {
            MyCollection<string> coll = new MyCollection<string>
            {
                "First element",
                "Second element",
                "Third element"
            };


            coll.CollectionChanged += printEvent;

            foreach (string el in coll)
                Console.WriteLine(el);

            Console.ReadLine();

            coll.Add("4-th element");
            coll[3] = "другой 4-й";
            Console.WriteLine(coll[3]);

            foreach (string el in coll)
                Console.WriteLine(el);

            coll.Remove("First element");

            foreach (string el in coll)
                Console.WriteLine(el);


            //mycollection<int> coll = new mycollection<int> { 2, 8, 3, 9, 3 };
            //foreach (int el in coll)
            //    console.write(el + " ");

            //console.readline();

            //mycollection<int> coll2 = new mycollection<int> { 2, 8, 3, 9, 3 };
            //foreach (int el in coll2)
            //    console.write(el + " ");

            //coll2.remove(9);
            //foreach (int el in coll2)
            //    console.write(el + " ");

            Console.ReadLine();

        }

        static void printEvent(object sender, MyCollectionEventArgs e)
        {
            Console.Write("===Event: ");
            switch (e.eventType)
            {
                case MyCollectionEventArgs.EventType.Added:
                    Console.Write("New element added");
                    break;

                case MyCollectionEventArgs.EventType.Deleted:
                    Console.Write("Element(s) deleted");
                    break;

                case MyCollectionEventArgs.EventType.Get:
                    Console.Write("Got element");
                    break;

                case MyCollectionEventArgs.EventType.Updated:
                    Console.Write("Collection updated");
                    break;
            }
            Console.WriteLine("===");

        }
    }
}
