﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Text.RegularExpressions;


namespace Task1._2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }


        string[] input;//массив со строками
        static string[] output;//выходной массив

        static string final;//Переменная, которую будем подставлять в TextBox в правой части формы

        /// <summary>
        /// GetInputData - разбивает строку st на подстроки, используя в качестве разделителя \n
        /// и сохраняет результат разбиения в массиве input. Каждый элемент обработанной строки st - 
        /// это отдельный элемент массива input
        /// </summary>
        /// <param name="st">Входная строка, которую необходимо преобразовать.</param>
        /// <remarks>
        /// Массив input[] объявлен в этом классе выше
        /// </remarks>
        public void GetInputData(string st)
        {
            
            input = st.Split('\n');
        }


        /// <summary>
        /// changeData - добавляет в массив координат символы X:_, и делает замену символов "," на Y:_ , "." на ","
        /// а также добавляет символ переноса строки
        /// </summary>
        /// <param name="input_txt">Входная строка, которую необходимо преобразовать.</param>
        public void changeData(string[] input_txt)
        {
            output = input_txt;

            for (int i = 0; i < output.Length-1; i++)
            {
                output[i] = output[i].Insert(0, "X: ");
                output[i] = output[i].Replace(",", " Y: ");
                output[i] = output[i].Replace(".", ",");
                output[i] += "\n";

                final += output[i];
            }

            //Эти 3 операции делаем для того, чтобы при обработке последней строки не добавлялось \n , а то на выходе получиться 4-я строка где будет добавлено X: и больше ничего
            output[output.Length-1] = output[output.Length - 1].Insert(0, "X: ");
            output[output.Length - 1] = output[output.Length - 1].Replace(",", " Y: ");
            output[output.Length - 1] = output[output.Length - 1].Replace(".", ",");

            //final = final.TrimEnd('\n');
        }


        private void button_Copy_Click(object sender, RoutedEventArgs e)
        {
            GetInputData(inputTxt.Text);
            changeData(input);
            outputTxt.Text = final;
        }


        /// <summary>
        /// PrintData - добавляет содержимое текстового файла в текстовое поле
        /// </summary>
        /// <param name="input">Входной массив, который будет отображён в текстовом поле</param>
        public void PrintData(string[] input)
        {
            inputTxt.Text = "";
            foreach (var elem in input)
                inputTxt.Text += elem + Environment.NewLine;
        }


        private void button_Click(object sender, RoutedEventArgs e)
        {
            // Настройка file dialog box
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.FileName = "Document"; // Имя файла по-умолчанию
            dlg.DefaultExt = ".txt"; // Расширение файла по-умолчанию
            dlg.Filter = "Text documents (.txt)|*.txt"; // Фильтрация файлов по расширению

            // Показываем диалоговое окно
            Nullable<bool> result = dlg.ShowDialog();

            // Обработка результатов диалогового окна
            if (result == true)
            {
                // Open document
                string filename = dlg.FileName;
                sourceTxt.Text = dlg.SafeFileName;

                //Читаем наш выбранный файл
                input = File.ReadAllLines(filename, System.Text.Encoding.Default);
                PrintData(input);
            }
        }

        private void inputTxt_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void inputTxt_KeyDown(object sender, KeyEventArgs e)
        {

        }
    }
}
