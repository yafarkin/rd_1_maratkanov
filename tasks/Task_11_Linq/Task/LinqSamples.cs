﻿// Copyright © Microsoft Corporation.  All Rights Reserved.
// This code released under the terms of the 
// Microsoft Public License (MS-PL, http://opensource.org/licenses/ms-pl.html.)
//
//Copyright (C) Microsoft Corporation.  All rights reserved.

using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using SampleSupport;
using Task;
using Task.Data;

// Version Mad01

namespace SampleQueries
{
    [Title("LINQ Module")]
    [Prefix("Linq")]
    public class LinqSamples : SampleHarness
    {
        private readonly DataSource dataSource = new DataSource();

        private bool ValidateZipCode(string val)
        {
            long number;
            return long.TryParse(val, out number);
        }

        private bool ValidatePhone(string phone)
        {
            var pattern = @"^(\([0-9]\))+?";
            return Regex.IsMatch(phone, pattern);
        }

        private List<GroupPriceEntity> SortProductsByPrice()
        {
            var sortedProducts = new List<GroupPriceEntity>();

            foreach (var prod in dataSource.Products)
                if (prod.UnitPrice <= 20M)
                    sortedProducts.Add(new GroupPriceEntity {product = prod, Group = 0});
                else if ((prod.UnitPrice > 20M) && (prod.UnitPrice <= 50M))
                    sortedProducts.Add(new GroupPriceEntity {product = prod, Group = 1});
                else
                    sortedProducts.Add(new GroupPriceEntity {product = prod, Group = 2});

            return sortedProducts;
        }


        [Category("Restriction Operators")]
        [Title("Task 001")]
        [Description("Выдайте список всех клиентов, чей суммарный оборот (сумма всех заказов) превосходит некоторую величину X. Продемонстрируйте выполнение запроса с различными X (подумайте, можно ли обойтись без копирования запроса несколько раз)")
        ]
        public void Linq001()
        {
            //инициализация

            //запрос
            var customers = dataSource.Customers.Select(c => c)
                .Where(c => c.Orders.Sum(ord => ord.Total) > 50000);
            //выдача результатов
            ObjectDumper.Write(customers);

            ObjectDumper.Write("\n");

            //запрос
            customers = dataSource.Customers.Select(c => c)
                .Where(c => c.Orders.Sum(ord => ord.Total) > 10000);
            //выдача результатов
            ObjectDumper.Write(customers);

            ObjectDumper.Write("\n");

            //запрос
            customers = dataSource.Customers.Select(c => c)
                .Where(c => c.Orders.Sum(ord => ord.Total) > 20000);
            //выдача результатов
            ObjectDumper.Write(customers);
        }


        [Category("Restriction Operators")]
        [Title("Task 002")]
        [Description("Для каждого клиента составьте список поставщиков, находящихся в той же стране и том же городе. Сделайте задания с использованием группировки и без.")
        ]
        public void Linq002()
        {
            var customers = dataSource.Customers.Join(
                dataSource.Suppliers,
                cus => cus.Country,
                sup => sup.Country,
                (cus, sup) => new { cus, sup })
                .Where(city => city.cus.City == city.sup.City)
                .Select(client => client.cus.CompanyName + client.sup.SupplierName);

            ObjectDumper.Write(customers);
        }

        [Category("Restriction Operators")]
        [Title("Task 003")]
        [Description("Найдите всех клиентов, у которых были заказы, превосходящие по сумме величину X")
        ]
        public void Linq003()
        {
 
            var customers = dataSource.Customers.SelectMany(cus => cus.Orders.Where(ord => ord.Total > 2000),
                (cus, ord) => new { cus.CustomerID, ord.Total });

            ObjectDumper.Write(customers);

        }


        [Category("Restriction Operators")]
        [Title("Task 004")]
        [Description("Выдайте список клиентов с указанием, начиная с какого месяца какого года они стали клиентами (принять за таковые месяц и год самого первого заказа)")
        ]
        public void Linq004()
        {

            var customers = dataSource.Customers.SelectMany(cus => cus.Orders
            .Select(ord => ord.OrderDate).Take(1));

            ObjectDumper.Write(customers);

        }


        [Category("Restriction Operators")]
        [Title("Task 005")]
        [Description("Сделайте предыдущее задание, но выдайте список отсортированным по году, месяцу, оборотам клиента (от максимального к минимальному) и имени клиента")]
        public void Linq005()
        {
            
            var customers = dataSource.Customers.SelectMany(cus => cus.Orders.Select(ord => ord.OrderDate).Take(1));
        
            //не знаю что сюда писать
                
            ObjectDumper.Write(customers);
        }



        [Category("Restriction Operators")]
        [Title("Task 006")]
        [Description("Укажите всех клиентов, у которых указан нецифровой код или не заполнен регион или в телефоне не указан код оператора (для простоты считаем, что это равнозначно «нет круглых скобочек в начале»)")]
        public void Linq006()
        {
            int res;

            var customers = dataSource.Customers
                .Where(cus => !int.TryParse(cus.PostalCode, out res) || (cus.Region == null) || !(cus.Phone.Contains('(')));

            ObjectDumper.Write(customers);
        }

        [Category("Restriction Operators")]
        [Title("Task 007")]
        [Description("Сгруппируйте все продукты по категориям, внутри – по наличию на складе, внутри последней группы отсортируйте по стоимости")]
        public void Linq007()
        {
          

            var prod = dataSource.Products
                .GroupBy(cat => cat.Category)
                .Select(sklad => sklad.GroupBy(cat => cat.UnitsInStock)
                .Select(price => price.OrderBy(cat => cat.UnitPrice)));
                
                ;

            ObjectDumper.Write(prod);
        }


        [Category("Restriction Operators")]
        [Title("Task 008")]
        [Description("Сгруппируйте товары по группам «дешевые», «средняя цена», «дорогие». Границы каждой группы задайте сами")]
        public void Linq008()
        {
            var products = SortProductsByPrice().GroupBy(p => p.Group);

            foreach (var key in products)
                foreach (var val in key)
                    ObjectDumper.Write(val.product);

        }



        [Category("Restriction Operators")]
        [Title("Task 1")]
        [Description("Описание задачи на русском языке")
        ]
        public void LinqForExample()
        {
            var customers = dataSource
                .Customers
                .Select(c => c);

            //Он умеет выполнять запросы самостоятельно. Так же умеет делать foreach по коллекции. 
            //Проверьте это все запустив приложение и сделав запуск запроса
            ObjectDumper.Write(customers);
        }
    }
}