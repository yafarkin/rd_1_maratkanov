﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Task2._2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            if(CorrectNumber(textDecimal.Text))
            {
                Converter myConv = new Converter(Convert.ToUInt32(textDecimal.Text));
                textBinary.Text = myConv.perevod(Convert.ToInt32(myConv.Dec)).ToString();
            }
            else
                textBinary.Text = "Введите корректное число!";

            //Converter myConv = new Converter(Convert.ToUInt32(textDecimal.Text));
            //textBinary.Text = myConv.perevod(Convert.ToInt32(myConv.Dec)).ToString();
        }


        /// <summary>
        /// Метод CorrectNumber проверяет вводимую строку на корректность
        /// </summary>
        /// <param name="st">Входная строка</param>
        /// <returns>true - строка корректна, false - строка не корректна</returns>
        bool CorrectNumber(string st)
        {
            int number;
            try
            {
                if (Int32.TryParse(st, out number))
                {
                    if (Convert.ToInt32(st) >= 0)
                        return true;
                    else
                        return false;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
    }
}
