﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2._2
{
    public class Converter
    {
        uint dec;
        string bin;

        public uint Dec
        {
            get
            {
                return dec;
            }
            set
            {
                dec = value;
            }
        }

        public string Bin
        {
            get
            {
                return bin;
            }
            set
            {
                bin = value;
            }
        }

        /// <summary>
        /// Converter - конструктор класса, инициализирует поле dec начальным десятичным значением
        /// </summary>
        /// <param name="input_dec">Начальное десятичное значение числа</param>
        public Converter(uint input_dec)
        {
            this.dec = input_dec;
        }

        //получает обратную запись двоичного числа из десятичного
        /// <summary>
        /// prevod - метод принимает десятичное число, и возвращает двоичное
        /// </summary>
        /// <param name="temp">Демятичное число</param>
        /// <returns>Двоичное представление десятичного числа</returns>
        public int perevod(int temp)
        {
            if (temp == 0)
                return 0;

            int temp1 = 0;
            List<int> s = new List<int>();
            while (temp > 0)
            {
                temp1 = temp % 2;
                temp = temp / 2;
                s.Add(temp1);
            }
            return obrat(s);
        }


        //переворачивает число и возвращает прямую запись двоичного числа.
        /// <summary>
        /// Вспомогательный метод, переворачивает порядок символов ддвоичного числа так, чтобы оно было правильным
        /// </summary>
        /// <param name="norm">Двоичное число, порядок символов которого нужно инвертировать</param>
        /// <returns>Готовое двоичное число, с правильным порядком следования символов</returns>
        public int obrat(List<int> norm)
        {
            int[] s = new int[norm.Count];
            for (int i = norm.Count - 1; i >= 0; i--)
            {
                s[norm.Count - 1 - i] = norm[i];
            }
            return Convert.ToInt32(string.Join<int>("", s));
        }

    }
}
