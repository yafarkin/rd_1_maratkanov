﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static EFDBFirst.Models.HomeViewModels;

namespace EFDBFirst.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            News_DBEntities db = new News_DBEntities();

            IndexModel model = new IndexModel
            {
                Articles = db.Article.ToList()
            };

            return View(model);
            
        }

        [HttpGet]
        public ActionResult AddNews()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddNews(AddNewsModel model)
        {
            if (ModelState.IsValid)
            {
                News_DBEntities db = new News_DBEntities();
                                
                db.Article.Add(model.Article);
                db.SaveChanges();
                return RedirectToAction("Index", "Home");
            }
            return View(model);
        }


        [HttpPost]
        public ActionResult AddComment(AddNewsAndComments model, int id)
        {
            if (ModelState.IsValid)
            {
                News_DBEntities db = new News_DBEntities();

                db.Comment.Add(model.Comment);
                db.SaveChanges();

                return RedirectToAction("DetailNews", "Home");
            }

            return View(model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpGet]
        public ActionResult DetailNews(int id)
        {
            News_DBEntities db = new News_DBEntities();
            DetailesModel model = new DetailesModel()
            {
                Article = db.Article.Where(c => c.ID == id).FirstOrDefault(),
                Comm = db.Comment.Where(d => d.ID == id).FirstOrDefault()
            };
        
            return View(model);
            
        }

        [HttpPost]
        public ActionResult DetailNews()
        {
            return View();
        }

        public ActionResult AddRecord()
        {
            News_DBEntities db = new News_DBEntities();
            List<Article> list = db.Article.ToList();//Статьи
            List<Comment> com_list = db.Comment.ToList();

            Article ar1 = new Article
            {
                head = "Новый заголовок",
                comment_FK = null,
                date = DateTime.Parse("04.04.2002")
            };

            list.Add(ar1);
            db.Article.Add(ar1);
            db.SaveChanges();

            return null;
        }

    }
}