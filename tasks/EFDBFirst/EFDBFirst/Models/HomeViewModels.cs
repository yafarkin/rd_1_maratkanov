﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EFDBFirst.Models
{
    public class HomeViewModels
    {
        public class AddNewsModel
        {   
            public Article Article { get; set; }
        }

        public class IndexModel
        {
           public List<Article> Articles { get; set; }
        }

        public class DetailesModel
        {
            public Article Article { get; set; }
            public Comment Comm { get; set; }//добавил как пробу 
        }

        public class AddCommentModel
        {
            public Comment Comment { get; set; }
        }

        public class AddNewsAndComments
        {
            public Article Article { get; set; }
            public Comment Comment { get; set; }
        }
    }
}