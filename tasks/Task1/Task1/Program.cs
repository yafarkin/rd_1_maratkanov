﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Task1
{
    class Program
    {
        static string[] input;
        static string[] output;

        static void GetData()
        {
            input = File.ReadAllLines("input.txt", System.Text.Encoding.Default);
        }

        static void PrintData(string[] input)
        {
            foreach (var elem in input)
            {
                Console.WriteLine(elem);
            }

            Console.ReadLine();
        }

        static void changeData(string[] input)
        {
            output = input;

            for(int i=0; i<output.Length;i++)
            {
                output[i] = output[i].Insert(0, "X: ");
                output[i] = output[i].Replace(",", " Y: ");
                output[i] = output[i].Replace(".", ",");
            }
        }
        

        static void Main(string[] args)
        {
            GetData();//считали данные из файла
            PrintData(input);//показали их на экране
            changeData(input);//обработали данные
            PrintData(output);//показали обработанные данные на экране
        }

        
    }
}
