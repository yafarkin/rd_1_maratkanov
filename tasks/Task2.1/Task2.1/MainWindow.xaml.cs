﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Task2._1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnCompute_Click(object sender, RoutedEventArgs e)
        {
            Root myRoot = new Root(textNumber.Text, textPower.Text, textPrecision.Text);

            /*
            myRoot.Power = Convert.ToDouble(textPower.Text);
            myRoot.Num = Convert.ToDouble(textNumber.Text);
            myRoot.Eps = Convert.ToDouble(textPrecision.Text);
            */

            textResult.Text = myRoot.GetRoot(myRoot.Power, myRoot.Num, myRoot.Eps).ToString();
            textResult_Pow.Text = Math.Pow(myRoot.Num, 1 / myRoot.Power).ToString();

        }
    }
}
