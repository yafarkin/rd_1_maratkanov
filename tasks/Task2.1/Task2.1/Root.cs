﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2._1
{
    /// <summary>
    /// Root - класс, позволяющий вычислять корень n-ой степени из числа методом Ньютона с заданной точностью
    /// </summary>
    public class Root
    {
        double num;
        double pow;
        double eps;

        public double Num
        {
            get
            {
                return num;
            }
            set
            {
                num = value;
            }
        }

        public double Power
        {
            get
            {
                return pow;
            }

            set
            {
                pow = value;
            }
        }

        public double Eps
        {
            get
            {
                return eps;
            }
            set
            {
                eps = value;
            }
        }

        public Root(string N, string root, string precision)
        {
            int res;
            double res_double;
            if(!int.TryParse(N, out res))
            {
                System.Windows.MessageBox.Show("Неверно введено число!");
            }
            else
                Num = Convert.ToInt32(N);


            if (!int.TryParse(root, out res))
            {
                System.Windows.MessageBox.Show("Неверно введена степень корня!");
            }
            else
                Power = Convert.ToInt32(root);

            if (!double.TryParse(precision, out res_double))
            {
                System.Windows.MessageBox.Show("Неверно введена точность!");
            }
            else
                Eps = Convert.ToDouble(precision);
        }


        /// <summary>
        /// GetPower - метод возводит параметр number в степень pow
        /// </summary>
        /// <param name="number">Число, которое нужно возвести в степень</param>
        /// <param name="pow">Степень, в которую возводится число number</param>
        /// <returns>Результат - number, возведенное в степень pow</returns>
        double GetPower(double number, int pow)
        {
            double result = 1;
            for (int i = 0; i < pow; i++)
                result *= number;
            
            return result;
        }

        //n - степень корня, А - само число, под знаком корня, eps - заданная точность
        /// <summary>
        /// GetRoot - метод определения корнястепени n, указанного числа A, с указанной точностью eps
        /// </summary>
        /// <param name="n">Степерь корня, в которую надо возвести число</param>
        /// <param name="A">Возводимое в степень корня число</param>
        /// <param name="eps">Точность подсчёта</param>
        /// <returns>Число А возведенное в степень 1/n </returns>
        public double GetRoot(double n, double A, double eps)
        {
            double x0 = A / n;
            double x1 = (1 / n) * ((n - 1) * x0 + A / GetPower(x0, (int)n - 1));

            while(Math.Abs(x1-x0)>eps)
            {
                x0 = x1;
                x1 = (1 / n) * ((n - 1) * x0 + A / GetPower(x0, (int)n - 1));
            }

            return x1;
        }
    }
}
