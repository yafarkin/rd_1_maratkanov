﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task7;

namespace Task7_ExceptionsTests
{
    [TestClass]
    public class MatrixTests
    {
        [TestMethod]
        public void isCompatibility_ThisFirst_2x2_Second_2x3_returnedEmptyString()
        {
            //arrange
            int[,] first_ar = new int[2, 2] { { 2, 5 }, { 1, 4 } };
            int[,] second_ar = new int[2, 3] { { 2, 5, 7 }, { 1, 4, 2 } };
            string expected = "";


            //act
            Matrix first = new Matrix(first_ar);
            Matrix second = new Matrix(second_ar);

            string actual = first.isCompatibility(second);


            //assert
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void Multiplication_first_2_5__1_4_second_2_5_7__1_4_2_returned_9_30_24__6_21_15()
        {
            //arrange
            int[,] first_ar = new int[2, 2] { { 2, 5 }, { 1, 4 } };
            int[,] second_ar = new int[2, 3] { { 2, 5, 7 }, { 1, 4, 2 } };
            int[,] expected = new int[2, 3] { { 9, 30, 24 }, { 6, 21, 15 } };

            //act
            Matrix first = new Matrix(first_ar);
            Matrix second = new Matrix(second_ar);

            Matrix result = new Matrix();
            result.M = result.Multiplication(first, second);
            int[,] actual = result.M;
            


            //assert
            CollectionAssert.AreEqual(expected, actual);
            //Assert.AreEqual(expected, actual);
            
        }
    }
}
