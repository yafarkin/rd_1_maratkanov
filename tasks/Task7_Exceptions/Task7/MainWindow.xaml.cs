﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Task7
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        Matrix first;
        Matrix second;
        Matrix result;

        private void button_Click(object sender, RoutedEventArgs e)
        {

            Random r = new Random(15);
            Random r2 = new Random(12);

            int n;

            //валидация
            if (int.TryParse(txtRows.Text, out n) && int.TryParse(txtCols.Text, out n) &&
                int.TryParse(txtRows2.Text, out n) && int.TryParse(txtCols2.Text, out n))
            {
                int[,] ar = new int[int.Parse(txtRows.Text), int.Parse(txtCols.Text)];
                int[,] ar2 = new int[int.Parse(txtRows2.Text), int.Parse(txtCols2.Text)];

                for (int i = 0; i < ar.GetLength(0); i++)
                    for (int j = 0; j < ar.GetLength(1); j++)
                        ar[i, j] = r.Next(15);

                for (int i = 0; i < ar2.GetLength(0); i++)
                    for (int j = 0; j < ar2.GetLength(1); j++)
                        ar2[i, j] = r2.Next(12);


                first = new Matrix(ar);
                second = new Matrix(ar2);

                txtMatrix1.Text = first.PrintMatrix(ar);
                txtMatrix2.Text = second.PrintMatrix(ar2);
            }
            else
            {
                txtMatrix1.Text = "Неверно заданы размеры матриц!";
            }

           

            
        }

        

        private void buttonMultiply_Click(object sender, RoutedEventArgs e)
        {
            txtMatrixResult.Clear();
            if (first.isCompatibility(second) == "")
            {
                //Сюда надо вставить наспечатку пеермноженной матрицы
                //txtMatrixResult.Text = "Умножаем";
                result = new Matrix();
                result.M = result.Multiplication(first, second);

                for (int i = 0; i < result.M.GetLength(0); i++)
                {
                    for (int j = 0; j < result.M.GetLength(1); j++)
                    {
                        txtMatrixResult.Text += result.M[i, j].ToString() + "  ";
                    }
                    txtMatrixResult.Text += "\n";
                }
                /*
                txtMatrixResult.Text += result.M[0, 0].ToString() + "  ";
                txtMatrixResult.Text += result.M[0, 1].ToString() + "\n";
                txtMatrixResult.Text += result.M[1, 0].ToString() + "  ";
                txtMatrixResult.Text += result.M[1, 1].ToString() + "\n";
                */
            }
            else
                txtMatrixResult.Text = first.isCompatibility(second);

            //first.Multiplication(first, second);
        }
    }
}
