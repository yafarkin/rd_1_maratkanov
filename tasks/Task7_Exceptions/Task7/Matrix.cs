﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task7
{
    /// <summary>
    /// Matrix - класс, для работы с матрицами.
    /// </summary>
    public class Matrix
    {
        
        List<List<int>> matrix = new List<List<int>>();      
        int[,] m;


        /// <summary>
        /// _Matrix - задает, или возвращает список, каждый элемент которого также 
        /// представляет собой список
        /// </summary>
        public List<List<int>> _Matrix
        {
            get { return matrix; }
            set { matrix = value; }
        }

        /// <summary>
        /// M - задает, или возвращает двумерный массив
        /// </summary>
        public int[,] M
        {
            get { return m; }
            set { m = value; }
        }

       
        
       /// <summary>
       /// Matrix(int[,] ar) - конструктор, инициализирует список List<List<int>> значениями
       /// из аргумента - массива ar
       /// </summary>
       /// <param name="ar">Двумерный массив, значения которого будут скопированы
       /// в List<List<int>>
       /// </param>
        public Matrix(int[,] ar)
        {
            List<int> temp;
            m = new int[ar.GetLength(0), ar.GetLength(1)];
            m = ar;
            for (int i = 0; i < ar.GetLength(0); i++)
            {
                temp = new List<int>();
                for (int j = 0; j < ar.GetLength(1); j++)
                {
                    temp.Add(ar[i, j]);//формируем одну строку матрицы
                }
                matrix.Add(temp);
            }
        }
        
        public Matrix()
        {

        }

        /// <summary>
        /// PrintMatrix(int [,]ar) - метод возвращает элементы массива ar[,]
        /// в виде строки (таблицы), удобной для отображения
        /// </summary>
        /// <param name="ar">Двумерный массив, который необходимо
        /// представить в виде строки
        /// </param>
        /// <returns>Строка в виде таблицы</returns>
        public string PrintMatrix(int [,]ar)
        {
            string res = "";

            for (int i = 0; i < ar.GetLength(0); i++)
            {
                for (int j = 0; j < ar.GetLength(1); j++)
                {
                    res += ar[i, j] + "     ";
                }
                res += "\n";
            }

            return res;
        }

        /// <summary>
        /// isCompatibility(Matrix second) - проверяет, совместима ли текущая матрица
        /// с той, что передана в качестве аргумента
        /// </summary>
        /// <param name="second">Матрица - аргумент, с которой идёт сравнение</param>
        /// <returns>Возвращает сообщение "Матрицы не совместимы!" в случае не совместимости
        /// матриц, или пустую строку, в случае, если матрицы совместимы
        /// </returns>
        public string isCompatibility(Matrix second)
        {
            string result="";
            try
            {
                if (this._Matrix[0].Count != second._Matrix.Count)
                {
                    throw new Exception("Матрицы не совместимы!");
                }
            }
            catch(Exception e)
            {
                result = e.Message;
            }

            return result;
        }

        /// <summary>
        /// Multiplication(Matrix first, Matrix second) - метод, для умножения двух матриц.
        /// </summary>
        /// <param name="first">1-я матрица</param>
        /// <param name="second">2-я матрица</param>
        /// <returns>Произведение двух матриц</returns>
        public int[,] Multiplication(Matrix first, Matrix second)
        {
            int[,] result = new int[first.M.GetLength(0), second.M.GetLength(1)];
            for (int i = 0; i < first.M.GetLength(0); i++)    
                for (int j = 0; j < second.M.GetLength(1); j++)
                    for(int k=0; k<second.M.GetLength(0); k++)
                    {
                        result[i, j] += first.M[i, k] * second.M[k, j];
                    }
            
            return result;
        }
        

        
    }
}
