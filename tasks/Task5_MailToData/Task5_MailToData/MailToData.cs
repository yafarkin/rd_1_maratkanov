﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task5_MailToData
{
    /// <summary>
    /// MailToData - класс, который описывает Имя и адрес человека (Город, область, почтовый индекс), и предоставляет метод для ввода данных
    /// в соответствующие свойства, а также метод для объединения этих свойств и выдаче их пользователю
    /// </summary>
    class MailToData
    {
        string name;
        string city;
        string state;
        int zip;

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public string City
        {
            get
            {
                return city;
            }
            set
            {
                city = value;
            }
        }

        public string State
        {
            get
            {
                return state;
            }

            set
            {
                state = value;
            }
        }

        public int Zip
        {
            get
            {
                return zip;
            }
            set
            {
                zip = value;
            }
        }

        /// <summary>
        /// GetData - считывае входную строку и разбивает её на отдельные данные - Город, область и почтовый индекс, и отдельно Имя
        /// </summary>
        /// <param name="address">Входная строка, которая может содержать в себе Город, Область и почтовый индекс,
        /// отделение одного свойства от другого производится по символу ","
        /// </param>
        /// <param name="name">Имя пользователя</param>
        public void GetData(string address, string name)
        {
            //Надо решить проблему как парсить строку, если в ней введены не все параметры
            if (address.Length != 0)
            {
                string[] input = address.Split(new char[] { ',' });

                switch (input.Length)
                {
                    case 1:
                        City = input[0].Trim();
                        State = "нет данных";
                        break;
                    case 2:
                        City = input[0].Trim();
                        State = input[1].Trim();
                        break;
                    case 3:
                        City = input[0].Trim();
                        State = input[1].Trim();
                        input[2] = input[2].Trim();

                        int i = 0;
                        while (i < input[2].Length)
                        {
                            if(!char.IsDigit(input[2][i]))
                            {
                                System.Windows.MessageBox.Show("Неверно введен почтовый индекс!");
                                break;
                            }
                            i++;

                            if (i == input[2].Length)
                                Zip = Convert.ToInt32(input[2].Trim());
                        }
                        
                        break;
                }
            }
            else
            {
                City = "нет данных";
                State = "нет данных";
            }

            if (name != "")
            {
                int i = 0;
                while(i<name.Length)
                {

                    if(Char.IsDigit(name[i]))
                    {
                        System.Windows.MessageBox.Show("Неверно введено Имя!");
                        break;
                    }
                    i++;
                }

                if (i == name.Length)
                    Name = name;
            }
            else
                Name = "нет данных";



        }

        /// <summary>
        /// Combine() - метод для объединения свойств "Имя", "Город", "Область", "Почтовый индекс" и выдачи этих свойств пользователю.
        /// </summary>
        /// <returns>Склеенная строка, содержащая в себе свойства "Имя", "Город", "Область", "Почтовый индекс"</returns>
        public string Combine()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(Name + "\n");
            sb.Append(City+"\n");
            sb.Append(State + "\n");
            sb.Append(Zip + "\n");

            return sb.ToString();
        }
    }
}
