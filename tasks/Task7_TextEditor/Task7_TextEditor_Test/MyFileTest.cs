﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task7_TextEditor;
using System.IO;

namespace Task7_TextEditor_Test
{
    [TestClass]
    public class MyFileTest
    {
        [TestMethod]
        //Проверяем метод считывания содержимого тестового файла
        public void ReadFile_input_abc_return_abc()
        {
            //arrange
            string expected = "abc";
            

            //act
            MyFile file = new MyFile("Test.txt");
            file.SR = new StreamReader("Test.txt");
            

            string actual = file.ReadFromFile(file.Path);
            //file.SR.Close();

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void WriteFile_input_123()
        {
            //arrange
            string expected = "out text";


            //act
            MyFile file = new MyFile("Out_text.txt");
            file.SW = new StreamWriter("Out_text.txt", false);
            file.WriteToFile(expected);//Записываем данные в файл

            //Чтоб проверить надо считать содержимое Out_text.txt и сравнить его с expected
            file.SR = new StreamReader("Out_text.txt");
            string actual = file.ReadFromFile("Out_text.txt");


            //assert
            Assert.AreEqual(expected, actual);

        }
    }
}
