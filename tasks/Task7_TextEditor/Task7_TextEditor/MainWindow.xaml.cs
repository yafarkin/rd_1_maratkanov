﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using Microsoft.Win32;

namespace Task7_TextEditor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            
        }

        MyFile file;
        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Text Files(*.txt)|*.txt";
            if (openFileDialog.ShowDialog() == true)
            {
                file = new MyFile(openFileDialog.FileName);
                file.SR = new StreamReader(openFileDialog.FileName);
                txtEditor.Text = file.ReadFromFile(file.Path);
                file.SR.Close();
            }
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            if (file != null)
            {
                try
                {
                    file.SW = new StreamWriter(file.Path, false);//true - дозапись в уже существующий файл. false - перезапись файла
                    file.WriteToFile(txtEditor.Text);
                }
                catch(UnauthorizedAccessException ex)
                {
                    System.Windows.MessageBox.Show("Ошибка! Файл только для чтения!");
                }
                
            }
            else
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.FileName = "Document";//Имя файла по-умолчанию
                saveFileDialog.DefaultExt = ".txt";//Расширение по-умолчанию
                saveFileDialog.Filter = "Текстовые файлы (.txt)|*.txt";

                
                // Process save file dialog box results
                if (saveFileDialog.ShowDialog() == true)
                {
                    // Сохранение документа
                    file = new MyFile(saveFileDialog.FileName);
                    file.SW = new StreamWriter(saveFileDialog.FileName);
                    file.WriteToFile(txtEditor.Text);
                }
            }
        }

        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
