﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Microsoft.Win32;

namespace Task7_TextEditor
{
    /// <summary>
    /// MyFile - класс для работы с файлами на жестком диске
    /// </summary>
    public class MyFile
    {
        string path;//Путь до файла
        string inputText;//Весь текст из считанного файла
        string outputText;


        StreamReader sr;
        StreamWriter sw;

        /// <summary>
        /// MyFile - конструктор класса MyFile
        /// </summary>
        /// <param name="path">Путь до файла, который открываем, или в который записываем
        /// данные
        /// </param>
        /// 

        
        public MyFile(string path)
        {
            this.path = path;
        }

        public MyFile()
        {
        }

        #region properties
        /// <summary>
        /// Path - путь до файла
        /// </summary>
        public string Path
        {
            get { return path; }
            set { path = value; }
        }

        /// <summary>
        /// Содержимое открываемого файла
        /// </summary>
        public string InputText
        {
            get { return inputText; }
            set { inputText = value; }
        }


        /// <summary>
        /// Текст для записи
        /// </summary>
        public string OutputText
        {
            get { return outputText; }
            set { outputText = value; }
        }


        /// <summary>
        /// SR - поток для чтения StreamReader
        /// </summary>
        public StreamReader SR
        {
            get { return sr; }
            set { sr = value; }
        }


        /// <summary>
        /// Поток для записис StreamWriter
        /// </summary>
        public StreamWriter SW
        {
            get { return sw; }
            set { sw = value; }
        }

        #endregion


        /// <summary>
        /// ReadFromFile - метод считывает текстовые данные из файла, и возвращает
        /// считанный текст
        /// </summary>
        /// <param name="path">Путь до файла</param>
        /// <returns>Текстовое содержимое файла</returns>
        public string ReadFromFile(string path)
        {
            InputText = SR.ReadToEnd();
            SR.Close();
            return InputText;
        }


        /// <summary>
        /// WriteToFile - метод для записи текстовых данных в файл
        /// </summary>
        /// <param name="content">Данные, которые будут записаны в файл</param>
        public void WriteToFile(string content)
        {
            SW.Write(content);
            SW.Close();
        }
    }
}
