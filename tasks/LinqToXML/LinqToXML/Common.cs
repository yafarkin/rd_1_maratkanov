﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace LinqToXML
{
    [Serializable()]
    public class Common
    {
        [XmlAttribute("isPoison")]
        public bool IsPoison { get; set; }
        [XmlText]
        public string Text { get; set; }
    }
}
