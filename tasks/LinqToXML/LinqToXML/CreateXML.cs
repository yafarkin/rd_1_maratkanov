﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace LinqToXML
{
    public class CreateXML
    {
        //задаем путь к нашему рабочему файлу XML
        string fileName = "base.xml";

        
        XDocument document = new XDocument(
            new XElement("CATALOG",
                new XElement("PLANT",
                    new XElement("COMMON",
                        new XAttribute("isPoison", "true"),"Cowslip"),
                    new XElement("BOTANICAL", "Caltha palustris"),
                    new XElement("ZONE", 4),
                    new XElement("LIGHT", "Mostly Shady"),
                    new XElement("PRICE",
                        new XAttribute("dimension", "dollars"),9.90),
                    new XElement("AVAILABILITY", 10)),
                new XElement("PLANT",
                    new XElement("COMMON",
                        new XAttribute("isPoison","false"),
                        "Dutchman's-Breeches"),
                    new XElement("BOTANICAL", "Dicentra cucullaria"),
                    new XElement("ZONE", 3),
                    new XElement("LIGHT", "Mostly Shady"),
                    new XElement("PRICE",
                        new XAttribute("dimension", "dollars"),6.44),
                    new XElement("AVAILABILITY", 234)),
                new XElement("PLANT",
                    new XElement("COMMON",
                        new XAttribute("isPoison", "true"),
                        "Ginger, Wild"),
                    new XElement("BOTANICAL", "Asarum canadense"),
                    new XElement("ZONE", 3),
                    new XElement("LIGHT", "Mostly Shady"),
                    new XElement("PRICE",
                        new XAttribute("dimension", "euros"),9.03),
                    new XElement("AVAILABILITY", 3)),
                new XElement("PLANT",
                    new XElement("COMMON",
                        new XAttribute("isPoison", "false"),
                        "Hepatica"),
                    new XElement("BOTANICAL", "Hepatica americana"),
                    new XElement("ZONE", 4),
                    new XElement("LIGHT", "Mostly Shady"),
                    new XElement("PRICE",
                        new XAttribute("dimension", "dollars"),4.45),
                    new XElement("AVAILABILITY", 56)),
                new XElement("PLANT",
                    new XElement("COMMON",
                        new XAttribute("isPoison", "false"),"Liverleaf"),
                    new XElement("BOTANICAL", "Hepatica americana"),
                    new XElement("ZONE", 4),
                    new XElement("LIGHT", "Mostly Shady"),
                    new XElement("PRICE",
                        new XAttribute("dimension", "euros"),3.99),
                    new XElement("AVAILABILITY", 37)))
                  );

        public void SaveXML()
        {
            document.Save(fileName);
        }
            
       
        
    }//class
}//namespace
