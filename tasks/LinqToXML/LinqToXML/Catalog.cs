﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace LinqToXML
{
    [Serializable]
    [XmlRoot("CATALOG")]
    public class Catalog
    {
        [XmlElement("PLANT")]
        public Plant[] plant { get; set; }
    }
}
