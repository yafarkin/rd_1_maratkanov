﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;

namespace LinqToXML
{
    public class Serialize
    {

        

        
        /// <summary>
        /// Метод сериализует объект Object
        /// в xml файл source
        /// </summary>
        /// <param name="Object"></param>
        /// <param name="source"></param>
        public void Ser(object Object, string source)
        {
            if (Object == null) { return; }

            using (var fs = new FileStream(source, FileMode.OpenOrCreate))
            {
             XmlSerializer formatter = new XmlSerializer(typeof(Catalog[]));
                formatter.Serialize(fs, Object);
            }
        }


        /// <summary>
        /// Метод возвращает десериализованный объект
        /// из XML файла source
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public object Deserialize(string source)
        {
            object obj;
            XmlSerializer DeSerialized = new XmlSerializer(typeof(Catalog));
            using (var fs = new FileStream(source, FileMode.OpenOrCreate))
            {
                obj = DeSerialized.Deserialize(fs);
            }

            return obj;
        }


    }
}
