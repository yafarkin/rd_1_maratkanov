﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace LinqToXML
{
    [Serializable]
    public class Plant
    {
        [XmlElement("COMMON")]
        public Common common { get; set; }

        [XmlElement("BOTANICAL")]
        public string Botanical { get; set; }

        [XmlElement("ZONE")]
        public int Zone { get; set; }

        [XmlElement("LIGHT")]
        public string Light { get; set; }

        [XmlElement("PRICE")]
        public Price price { get; set; }

        [XmlElement("AVAILABILITY")]
        public int Availability { get; set; }
    }
}
