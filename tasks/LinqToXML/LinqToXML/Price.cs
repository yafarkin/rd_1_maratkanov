﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace LinqToXML
{
    [Serializable]
    public class Price
    {
        [XmlText()]
        public double price { get; set; }
        [XmlAttribute("dimension")]
        public string dimension { get; set; }
    }
}
